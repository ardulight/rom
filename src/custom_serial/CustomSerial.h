#pragma once

#include <Arduino.h>
#include "ArduinoJson.h"
#include "../effects/Effect.h"

class CustomSerial {
    private:
        String readedString = "";
        byte openedBracketsCount = 0;

    public:
        CustomSerial() {
        }

        String readString() {
            while (Serial.available() > 0) {
                
                char c = Serial.read();
                readedString += c;

                if (c == '{' || c == '[') {
                    openedBracketsCount++;
                }
                else if (c == '}' || c == ']') {
                    openedBracketsCount--;
                    String finalString = readedString;
                    readedString = "";
                    if (openedBracketsCount == 0) {
                        return finalString;
                    }
                }
            }

            return "";
        }

        byte claculateHash(String s){

        }
};
#pragma once

#include <Arduino.h>
#include "ArduinoJson.h"
#include "../collections/list/ArrayList.h"
#include "FastLED.h"

class Effect {
    public:
        virtual String name();
        virtual String widgets();
        virtual void init(String json);
        virtual void destroy();
        virtual void loop(CRGB leds[], byte count);
};
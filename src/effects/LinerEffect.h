#pragma once

#include <Arduino.h>
#include "ArduinoJson.h"
#include "./Effect.h"
#include "../collections/list/ArrayList.h"
#include "FastLED.h"

class LinerEffect: public Effect{
    private:
        CRGBPalette16 _currentPalette = RainbowColors_p;           
        TBlendType _currentBlending = LINEARBLEND;
        byte _delay = 10;

    public:
        LinerEffect() {

        }

        String name() {
            return F("Liner");
        }
        
        String widgets() {
            return F(R"(
[
    "reverse": {
        "type": "checkBoxWidget",
        "value": true
    },
    "speed": {
        "type": "sliderWidget",
        "min": 0,
        "max": 255,
        "value": 100
    },
    "effect": {
        "type": "selectWidget",
        "items": [
        {
            "title": "Party"
        },
        {
            "title": "Rainbow"
        },
        {
            "title": "Forest"
        },
        {
            "title": "Sky"
        },
        {
            "title": "Sea"
        }],
        "selectedIndex": 1
    }
])");
        }

        virtual void init(String json){
            
        }

        void destroy() {}
        
        void loop(CRGB leds[], byte count) {
            static uint8_t startIndex = 0;
            startIndex = startIndex - 1;
            FillLEDsFromPaletteColors(leds, count, startIndex);
            FastLED.show();
            delay(_delay);
        };

        void FillLEDsFromPaletteColors(CRGB leds[], byte count, uint8_t colorIndex) {
            uint8_t brightness = 255;
            
            for( int i = 0; i < count; i++) {
                leds[i] = ColorFromPalette( _currentPalette, colorIndex, brightness, _currentBlending);
                colorIndex += 10;
            }
        }
};
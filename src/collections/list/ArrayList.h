#pragma once

#include <Arduino.h>

template<typename T>
class ArrayList {
    private:
        T* _items;
        byte _size;

    public:
        ArrayList() {
            _size = 0;
            _items = new T[_size];
        }

        ArrayList(T items[], byte size) {
            _size = size;
            _items = new T[_size];
            for (byte i = 0; i < _size; i++){
                _items[i] = items[i];
            }
        }

        template<byte size>
        ArrayList(T (&items)[size]) {
            _size = size;
            _items = new T[_size];
            for (byte i = 0; i < _size; i++){
                _items[i] = items[i];
            }
        }

        ~ArrayList() {
            delete [] _items;
        }

        T get(byte index) {
            return _items[index];
        }

        byte size() {
            return _size;
        }
};
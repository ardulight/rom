#include <Arduino.h>
#include "./effects/Effect.h"
#include "./effects/LinerEffect.h"
#include "./service/ArdulightService.h"
#include "ArduinoJson.h"
#include <Vector.h>

#define LED_COUNT 30
#define LED_DT 8

struct CRGB leds[LED_COUNT];
ArdulightService service;

void setup() {
  Serial.begin(115200);
  LEDS.setBrightness(75);
  LEDS.addLeds<WS2812B, LED_DT, GRB>(leds, LED_COUNT);
  Effect* effects[] = {
    new LinerEffect(), 
  };
  service = ArdulightService(effects, leds);
  while (!Serial) continue;
  Serial.print("Start");
}

void loop() {
  service.loop();
}
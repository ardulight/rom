#pragma once

#include <Arduino.h>
#include "ArduinoJson.h"
#include "../effects/Effect.h"
#include "../custom_serial/CustomSerial.h"

class ArdulightService {
    private:
        ArrayList<Effect*> _effects;
        CRGB* _leds;
        byte _ledSize;
        byte _selected = 0;
        CustomSerial serial;

        void changeModeCheck() {
            String incomingString = serial.readString();
            if (incomingString == "")
                return;

            Serial.print(incomingString);
                /*DynamicJsonDocument doc = DynamicJsonDocument(100);
                DeserializationError error = deserializeJson(doc, incomingString);

                if (error) {
                    Serial.print(F("deserializeJson() failed: "));
                    Serial.println(error.f_str());
                }

                String mode = doc["mode"];

                if (mode == "setEffects") {
                    _selected = doc["value"];
                } else if (mode == "getWidgets") {
                    byte id = doc["id"];
                    Serial.println(widgets(id));
                }*/
        }

    public:
        ArdulightService() {
            serial = CustomSerial();
        }

        template<byte effectsSize, byte ledsSize>
        ArdulightService(Effect* (&effects)[effectsSize], CRGB (&leds)[ledsSize]) {
            serial = CustomSerial();
            _effects = ArrayList<Effect*>(effects);
            _leds = leds;
            _ledSize = ledsSize;
        }

        void loop() {
            changeModeCheck();
            //_effects.get(_selected)->loop(_leds, _ledSize);
        }

        /*void effectsNames(DynamicJsonDocument doc){
            for (byte i = 0; i < _effects.size(); i++){
                doc.add((_effects.get(i))->name());
            }
        }*/

        String widgets(byte index){
            return _effects.get(index)->widgets();
        }

        /*void getEffects(){
            DynamicJsonDocument doc(250);
            effectsNames(doc);
            serializeJson(doc, Serial);
        }*/
};